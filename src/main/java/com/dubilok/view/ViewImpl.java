package com.dubilok.view;

import com.dubilok.controller.Controller;
import com.dubilok.controller.ControllerImpl;
import com.dubilok.service.Locale;
import com.dubilok.util.UtilMenu;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ViewImpl implements View {

    private Controller controller;
    private Locale locale;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
    private static Logger logger = LogManager.getLogger(ViewImpl.class);

    public ViewImpl() {
        menu = new LinkedHashMap<>();
        locale = new Locale();
        controller = new ControllerImpl();
        locale.start();
        start();
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("0", this::pressButton0);
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("5", this::pressButton5);
        methodsMenu.put("6", this::pressButton6);
        methodsMenu.put("7", this::pressButton7);
        methodsMenu.put("8", this::pressButton8);
        methodsMenu.put("9", this::pressButton9);
        methodsMenu.put("10", this::pressButton10);
        methodsMenu.put("11", this::pressButton11);
        methodsMenu.put("12", this::pressButton12);
        methodsMenu.put("13", this::pressButton13);
        methodsMenu.put("14", this::pressButton14);
        methodsMenu.put("15", this::pressButton15);
        methodsMenu.put("16", this::pressButton16);
    }

    private void pressButton0() {
        new ViewImpl().show();
    }

    public void start() {
        menu.put("0", "  0 - Select language");
        menu.put("1", "  1 - " + locale.getMessages().getString("command.1"));
        menu.put("2", "  2 - " + locale.getMessages().getString("command.2"));
        menu.put("3", "  3 - " + locale.getMessages().getString("command.3"));
        menu.put("4", "  4 - " + locale.getMessages().getString("command.4"));
        menu.put("5", "  5 - " + locale.getMessages().getString("command.5"));
        menu.put("6", "  6 - " + locale.getMessages().getString("command.6"));
        menu.put("7", "  7 - " + locale.getMessages().getString("command.7"));
        menu.put("8", "  8 - " + locale.getMessages().getString("command.8"));
        menu.put("9", "  9 - " + locale.getMessages().getString("command.9"));
        menu.put("10", "  10 - " + locale.getMessages().getString("command.10"));
        menu.put("11", "  11 - " + locale.getMessages().getString("command.11"));
        menu.put("12", "  12 - " + locale.getMessages().getString("command.12"));
        menu.put("13", "  13 - " + locale.getMessages().getString("command.13"));
        menu.put("14", "  14 - " + locale.getMessages().getString("command.14"));
        menu.put("15", "  15 - " + locale.getMessages().getString("command.15"));
        menu.put("16", "  16 - " + locale.getMessages().getString("command.16"));
        menu.put("Q", "  Q - " + locale.getMessages().getString("command.0"));
    }

    private void pressButton1() {
        List<String> listWords = new ArrayList<>();
        while (true) {
            logger.info("Enter your word: ");
            try {
                String str = bufferedReader.readLine().toLowerCase();
                if (str.isEmpty()) {
                    break;
                }
                listWords.add(str);
            } catch (IOException e) {
                logger.error(e);
            }
        }
        String[] arrayString = new String[listWords.size()];
        for (int i = 0; i < listWords.size(); i++) {
            arrayString[i] = listWords.get(i);
        }
        controller.showFirstProblem(arrayString);
    }

    private void pressButton2() {
        controller.showSecondProblem();
    }

    private void pressButton3() {
        controller.showThirdProblem();
    }

    private void pressButton4() {
        logger.info("Enter your length: ");
        try {
            controller.showFourthProblem(Integer.parseInt(bufferedReader.readLine()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void pressButton5() {
        controller.showFifthProblem();
    }

    private void pressButton6() {
        controller.showSixthProblem();
    }

    private void pressButton7() {
        controller.showSeventhProblem();
    }

    private void pressButton8() {
        controller.showEightProblem();
    }

    private void pressButton9() {
        logger.info("Enter your letter: ");
        try {
            controller.showNinthProblem(bufferedReader.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void pressButton10() {
        controller.showTenthProblem("книга", "taras", "а");
    }

    private void pressButton11() {
        controller.showEleventhProblem('t', 's');
    }

    private void pressButton12() {
        logger.info("Enter your length: ");
        try {
            controller.showTwelfthProblem(Integer.parseInt(bufferedReader.readLine()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void pressButton13() {
        logger.info("Enter your letter: ");
        try {
            controller.showThirteenthProblem(bufferedReader.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void pressButton14() {
        controller.showFourteenthProblem();
    }

    private void pressButton15() {
        controller.showFifteenthProblem();
    }

    private void pressButton16() {
        try {
            logger.info("Enter your length: ");
            int length = Integer.parseInt(bufferedReader.readLine());
            logger.info("Enter your String for replace: ");
            String string = bufferedReader.readLine();
            controller.showSixteenthProblem("", length, string);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void show() {
        UtilMenu.show(bufferedReader, menu, methodsMenu);
    }
}
