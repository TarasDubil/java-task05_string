package com.dubilok.controller;

import com.dubilok.service.Manager;
import com.dubilok.service.ManagerImpl;

public class ControllerImpl implements Controller {

    private Manager manager;

    public ControllerImpl() {
        manager = new ManagerImpl();
    }

    @Override
    public void showFirstProblem(String...strings) {
        manager.firstProblem(strings);
    }

    @Override
    public void showSecondProblem() {
        manager.secondProblem();
    }

    @Override
    public void showThirdProblem() {
        manager.thirdProblem();
    }

    @Override
    public void showFourthProblem(int length) {
        manager.fourthProblem(length);
    }

    @Override
    public void showFifthProblem() {
        manager.fifthProblem();
    }

    @Override
    public void showSixthProblem() {
        manager.sixthProblem();
    }

    @Override
    public void showSeventhProblem() {
        manager.seventhProblem();
    }

    @Override
    public void showEightProblem() {
        manager.eightProblem();
    }

    @Override
    public void showNinthProblem(String letter) {
        manager.ninthProblem(letter);
    }

    @Override
    public void showTenthProblem(String...strings) {
        manager.tenthProblem(strings);
    }

    @Override
    public void showEleventhProblem(char start, char finish) {
        manager.eleventhProblem(start, finish);
    }

    @Override
    public void showTwelfthProblem(int length) {
        manager.twelfthProblem(length);
    }

    @Override
    public void showThirteenthProblem(String symbol) {
        manager.thirteenthProblem(symbol);
    }

    @Override
    public void showFourteenthProblem() {
        manager.fourteenthProblem();
    }

    @Override
    public void showFifteenthProblem() {
        manager.fifteenthProblem();
    }

    @Override
    public void showSixteenthProblem(String sentence, int length, String string) {
        manager.sixteenthProblem(sentence, length, string);
    }
}
