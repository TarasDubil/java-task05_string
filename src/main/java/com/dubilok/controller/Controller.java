package com.dubilok.controller;

public interface Controller {
    void showFirstProblem(String...strings);

    void showSecondProblem();

    void showThirdProblem();

    void showFourthProblem(int length);

    void showFifthProblem();

    void showSixthProblem();

    void showSeventhProblem();

    void showEightProblem();

    void showNinthProblem(String letter);

    void showTenthProblem(String...strings);

    void showEleventhProblem(char start, char finish);

    void showTwelfthProblem(int length);

    void showThirteenthProblem(String symbol);

    void showFourteenthProblem();

    void showFifteenthProblem();

    void showSixteenthProblem(String sentence, int length, String string);

}
