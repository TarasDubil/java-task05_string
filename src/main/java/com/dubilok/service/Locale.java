package com.dubilok.service;

import com.dubilok.util.UtilMenu;
import com.dubilok.view.Printable;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.ResourceBundle;

public class Locale {
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private ResourceBundle messages;
    private static BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

    public Locale() {
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - USA");
        menu.put("2", "  2 - Japanese");
        menu.put("3", "  3 - Ukraine");
        menu.put("Q", "  Q - exit");
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
    }

    private void pressButton1() {
        messages = ResourceBundle.getBundle("messages", new java.util.Locale("en"));
    }

    private void pressButton2() {
        messages = ResourceBundle.getBundle("messages", new java.util.Locale("ja"));
    }

    private void pressButton3() {
        messages = ResourceBundle.getBundle("messages", new java.util.Locale("uk"));
    }

    public void start() {
        UtilMenu.show(bufferedReader, menu, methodsMenu);
    }

    public ResourceBundle getMessages() {
        return messages;
    }
}
