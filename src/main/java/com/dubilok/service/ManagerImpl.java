package com.dubilok.service;

import com.dubilok.model.Sentence;
import com.dubilok.model.Word;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class ManagerImpl implements Manager {

    private Sentence sentence;
    private Word word;
    private static Logger logger = LogManager.getLogger(ManagerImpl.class);

    public ManagerImpl() {
        this.sentence = new Sentence();
        this.word = new Word();
    }

    @Override
    public void firstProblem(String...strings) {
        List<String> listData = Arrays.asList(strings);
        List<String> sentences = sentence.getSentences();
        List<String> result = new ArrayList<>();
        for (String s : sentences) {
            List<String> words = word.getWordsFromThisSentence(s);
            if (words.containsAll(listData)) {
                result.add(s);
            }
        }
        if (result.isEmpty()) {
            logger.info("There is not any sentences with these words" + Arrays.asList(strings).toString());
        } else {
            result.forEach(System.out::println);
        }
    }

    @Override
    public void secondProblem() {
        List<String> listAllSentences = sentence.getSentences();
        Map<String, Integer> map = new HashMap<>();
        for (String s : listAllSentences) {
            List<String> words = word.getWordsFromThisSentence(s);
            map.put(s, words.size());
        }
        map.entrySet().stream().sorted(Map.Entry.comparingByValue()).forEach(k -> logger.info(k.getKey()));
    }

    @Override
    public void thirdProblem() {
        Set<String> uniqueWords = new HashSet<>();
        List<String> wordsFirstSentences = word.getWordsFromThisSentence(sentence.getSentences().get(0));
        List<String> listAllWords = word.getAllWordsFromSentencesWithoutFirst();
        wordsFirstSentences.forEach(e -> {
            if (!listAllWords.contains(e)) {
                uniqueWords.add(e);
            }
        });
        logger.info("" + uniqueWords);
    }

    @Override
    public void fourthProblem(int length) {
        Set<String> set = new HashSet<>();
        List<String> questionMarkSentences = getQuestionMarkSentences();
        for (int i = 0; i < questionMarkSentences.size(); i++) {
            List<String> wordsFromThisSentence = word.getWordsFromThisSentence(questionMarkSentences.get(i));
            wordsFromThisSentence.stream().filter(e -> e.length() == length)
                    .forEach(set::add);
        }
        set.forEach(logger::info);
    }

    @Override
    public void fifthProblem() {
        StringBuilder result = new StringBuilder();
        List<String> allSentences = sentence.getSentences();
        for (String s : allSentences) {
            String theLongestWord;
            String firstWordInFirstVovelLetter = "";
            List<String> words = word.getWordsFromThisSentence(s);
            for (String w : words) {
                if (isVowelLetter(w.charAt(0))) {
                    firstWordInFirstVovelLetter = w;
                    break;
                }
            }
            words.sort((x, y) -> Integer.compare(y.length(), x.length()));
            theLongestWord = words.get(0);
            String replacePatternFrom = "\\b" + firstWordInFirstVovelLetter + "\\b";
            String replacePatternTo = "\\b" + theLongestWord + "\\b";
            s = s.replaceFirst(replacePatternFrom, "@");
            s = s.replaceFirst(replacePatternTo, firstWordInFirstVovelLetter);
            s = s.replaceFirst("@", theLongestWord);
            result.append(s).append(" ");
        }
        logger.info(result);
    }

    @Override
    public void sixthProblem() {
        List<String> words = word.getWords();
        Collections.sort(words);
        String letter = words.get(0).substring(0, 1);
        boolean redline = true;
        for (String word : words) {
            if (!word.substring(0, 1).equals(letter)) {
                redline = true;
                letter = word.substring(0, 1);
            }
            if (redline) {
                logger.info("\t" + word);
            } else {
                logger.info(word);
            }
            redline = false;
        }
    }

    @Override
    public void seventhProblem() {
        List<String> words = word.getWords();
        Map<String, Double> result = new HashMap<>();
        for (String s : words) {
            double sum = (getCountVowels(s) * 1.0 / s.length());
            result.put(s, sum);
        }
        result.entrySet().stream()
                .sorted(Map.Entry.comparingByValue())
                .forEach(k -> logger.info(k.getKey() + " - " + k.getValue()));
    }

    @Override
    public void eightProblem() {
        List<String> words = word.getWords();
        Set<String> firstVowelWords = words.stream()
                .filter(e -> isVowelLetter(e.charAt(0)))
                .collect(Collectors.toCollection(TreeSet::new));
        logger.info(firstVowelWords);
    }

    @Override
    public void ninthProblem(String letter) {
        List<String> words = word.getWords();
        Map<String, Long> map = new TreeMap<>();
        words.forEach(w -> map.put(w, getCountLetterInWord(w, letter)));
        map.entrySet().stream()
                .sorted(Map.Entry.comparingByValue())
                .forEach(e -> logger.info(e.getKey() + " " + e.getValue()));
    }

    @Override
    public void tenthProblem(String... strings) {
        List<String> sentences = sentence.getSentences();
        Map<String, Integer> map = new HashMap<>();
        for (String s : sentences) {
            System.out.println(s);
            List<String> words = word.getWordsFromThisSentence(s);
            for (String data : strings) {
                int count = Collections.frequency(words, data.toLowerCase());
                map.put(data, count);
            }
            map.entrySet().stream()
                    .sorted((Map.Entry.<String, Integer>comparingByValue().reversed()))
                    .forEach(e -> logger.info(e.getKey() + " " + e.getValue()));
            System.out.println();
            map.clear();
        }
    }

    @Override
    public void eleventhProblem(char start, char finish) {
        List<String> sentences = sentence.getSentences();
        StringBuilder result = new StringBuilder();
        for (String s : sentences) {
            List<String> words = word.getWordsFromThisSentence(s);
            List<String> forDeleting = new ArrayList<>();
            for (String w : words) {
                if (isWordStartFromAndFinish(w, start, finish)) {
                    forDeleting.add(w);
                }
            }
            forDeleting.sort(String::compareTo);
            Collections.reverse(forDeleting);
            if (!forDeleting.isEmpty()) {
                String delete = "\\b" + forDeleting.get(0) + "\\b";
                s = s.replaceAll(delete, "");
                result.append(s).append(" ");
            } else {
                result.append(s);
            }
        }
        logger.info(result.toString().trim().replaceAll("[  ]+", " "));
    }

    @Override
    public void twelfthProblem(int length) {
        List<String> list = word.getWords();
        List<String> wordForReplase = list.stream()
                .filter(s -> s.length() == length)
                .filter(s -> !isVowelLetter(s.charAt(0)))
                .collect(Collectors.toList());
        String result = sentence.getBook().getBook().toLowerCase();
        for (String s : wordForReplase) {
            String toReplace = "\\b" + s + "\\b";
            result = result.replaceAll(toReplace, "");
        }
        logger.info(result.replaceAll("[ ]+", " ").trim());
    }

    @Override
    public void thirteenthProblem(String symbol) {
        List<String> words = word.getWords();
        Map<String, Long> map = new TreeMap<>();
        words.forEach(w -> map.put(w, getCountLetterInWord(w, symbol)));
        map.entrySet().stream()
                .sorted((Map.Entry.<String, Long>comparingByValue().reversed()))
                .forEach(e -> logger.info(e.getKey() + " " + e.getValue()));
    }

    @Override
    public String fourteenthProblem() {
        String string = word.getWords()
                .stream()
                .filter(this::isPalindrome)
                .max(Comparator.comparingInt(String::length))
                .get();
        logger.info(string);
        return string;
    }

    @Override
    public void fifteenthProblem() {
        List<String> words = word.getWords();
        for (String s : words) {
            logger.info("Before: " + s);
            StringBuilder result = new StringBuilder();
            result.append(s.charAt(0));
            for (int i = 1; i < s.length(); i++) {
                if (!(s.charAt(i) == s.charAt(0))) {
                    result.append(s.charAt(i));
                }
            }
            logger.info("After: " + result + "\n");
        }
    }

    @Override
    public String sixteenthProblem(String sentence, int length, String string) {
        String newText = sentence;
        List<String> list = word.getWordsFromThisSentence(newText);
        List<String> wordForReplace = list.stream().filter(s -> s.length() == length)
                .collect(Collectors.toList());
        for (String s : wordForReplace) {
            String toReplase = "\\b" + s + "\\b";
            newText = newText.replaceAll(toReplase, string);
        }
        logger.info(newText);
        return newText;
    }

    private boolean isWordStartFromAndFinish(String word, char start, char finish) {
        if ((word.charAt(0) == start) && (word.charAt(word.length() - 1) == finish)) {
            return true;
        } else {
            return false;
        }
    }

    private boolean isVowelLetter(char letter) {
        Pattern p = Pattern.compile("[аоуеиі]");
        Matcher m = p.matcher(String.valueOf(letter));
        return m.find();
    }

    private boolean isVowelLetter(String letter) {
        Pattern p = Pattern.compile("[аоуеиіАОУЕИІaoeuiyAOEUIY]");
        Matcher m = p.matcher(String.valueOf(letter));
        return m.find();
    }

    private boolean isPalindrome(String word) {
        StringBuilder stringBuilder = new StringBuilder(word);
        stringBuilder.reverse();
        return word.equals(stringBuilder.toString());
    }

    private List<String> getQuestionMarkSentences() {
        List<String> questionMark = new ArrayList<>();
        sentence.getSentences().stream().filter(e -> e.endsWith("?"))
                .forEach(questionMark::add);
        return questionMark;
    }

    private long getCountVowels(String word) {
        return word.codePoints()
                .mapToObj(c -> String.valueOf((char) c))
                .filter(this::isVowelLetter)
                .count();
    }

    private long getCountLetterInWord(String word, String letter) {
        return word.codePoints()
                .mapToObj(c -> String.valueOf((char) c))
                .filter(w -> w.equals(letter))
                .count();
    }
}
