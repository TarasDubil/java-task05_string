package com.dubilok.service;

public interface Manager {
    void firstProblem(String...strings);

    void secondProblem();

    void thirdProblem();

    void fourthProblem(int length);

    void fifthProblem();

    void sixthProblem();

    void seventhProblem();

    void eightProblem();

    void ninthProblem(String letter);

    void tenthProblem(String...strings);

    void eleventhProblem(char start, char finish);

    void twelfthProblem(int length);

    void thirteenthProblem(String symbol);

    String fourteenthProblem();

    void fifteenthProblem();

    String sixteenthProblem(String sentence, int length, String string);
}
