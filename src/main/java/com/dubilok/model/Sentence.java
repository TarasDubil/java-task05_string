package com.dubilok.model;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Sentence {

    private List<String> sentences;
    private Book book;

    public Sentence() {
        this.book = new Book("text.txt");
        this.sentences = getSentencesFromBook(book.getBook());
    }

    private List<String> getSentencesFromBook(String string) {
        List<String> sentences = new ArrayList<>();
        Pattern p = Pattern.compile("[(A-ZА-ЯІЇЄ)|(\\—)]+[^;.?!]+[;.?!]+");
        Matcher m = p.matcher(string);
        while (m.find()){
            sentences.add(string.substring(m.start(),m.end()).trim());
        }
        return sentences;
    }

    public List<String> getSentences() {
        return sentences;
    }

    public Book getBook() {
        return book;
    }
}
