package com.dubilok.model;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Book {

    private String fileName;
    private String book;

    public Book(String fileName) {
        this.fileName = fileName;
        this.book = getStringFromFile();
    }

    private String getStringFromFile() {
        try {
            return Files.lines(Paths.get(fileName)).reduce((a, b) -> a + b)
                    .map(s -> s.replaceAll("\\s{2,}+", " ")).get();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String getBook() {
        return book;
    }
}
