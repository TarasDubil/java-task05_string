package com.dubilok.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Word {
    private List<String> words;
    private Sentence sentence;
    private Map<Integer, List<String>> map;

    public Word() {
        this.sentence = new Sentence();
        this.words = getAllWordsFromSentences();
        this.map = getMapWords();
    }

    private List<String> getAllWordsFromSentences() {
        List<String> sentences = sentence.getSentences();
        return getStrings(sentences);
    }

    public List<String> getAllWordsFromSentencesWithoutFirst() {
        List<String> sentences = sentence.getSentences();
        sentences.remove(0);
        return getStrings(sentences);
    }

    private List<String> getStrings(List<String> sentences) {
        List<String> list = sentences.stream().flatMap(e -> Stream.of(e.split(" ")))
                .flatMap(f -> Stream.of(f.replaceAll("[.,()»«!?—:;]", "").toLowerCase()))
                .filter(w -> !w.isEmpty())
                .collect(Collectors.toList());
        return list;
    }

    public List<String> getWordsFromThisSentence(String sentence) {
        List<String> list = Stream.of(sentence).flatMap(e -> Stream.of(e.split(" ")))
                .flatMap(f -> Stream.of(f.replaceAll("[.,()»«!?—:;]", "").toLowerCase()))
                .filter(w -> !w.isEmpty())
                .collect(Collectors.toList());
        return list;
    }

    private Map<Integer, List<String>> getMapWords() {
        Map<Integer, List<String>> map = new HashMap<>();
        List<String> sentences = sentence.getSentences();
        for (int i = 0; i < sentences.size(); i++) {
            map.put(i, getWordsFromThisSentence(sentences.get(i)));
        }
        return map;
    }

    public List<String> getWords() {
        return words;
    }

    public Sentence getSentence() {
        return sentence;
    }

    public Map<Integer, List<String>> getMap() {
        return map;
    }
}
